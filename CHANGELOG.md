## v1.2.2 (June 11, 2017)

* Include fix from v1.1.4 and v1.1.3

## v1.2.1 (May 13, 2017)

* Fix client side hydratation from server side rendering

## v1.2.0 (May 13, 2017)

* Add languages in global store, can get them with the `LANGUAGES` getter
* Update build system using Rollup
* Manage server-side-rendering

## v1.1.4 (May 22, 2017)

* return promise when call the `saveService`
* do not save the service on the order reload

## v1.1.3 (May 18, 2017)

* Reject the wrong card with all the card object

## v1.1.2 (May 9, 2017)

* Remove Vue dependency
* Refactor filters

## v1.1.1 (May 9, 2017)

* fix issue when include module

## v1.1.0 (May 8, 2017)

* Add support for restricted experts
* Add pagination support for feedbacks

## v1.0.0 (April 27, 2017)

* Creation of Skiliko Vue Core lib