import commonjs from 'rollup-plugin-commonjs'
import resolve from 'rollup-plugin-node-resolve'
import babel from 'rollup-plugin-babel'
import uglify from 'rollup-plugin-uglify'

const pkg = require('./package.json')

export default {
  entry: './src/index.js',
  targets: [
    {
      dest: pkg.main,
      format: 'umd',
      moduleName: 'SkilikoVueCore'
    }
  ],
  plugins: [
    resolve({ jsnext: true }),
    babel({
      exclude: 'node_modules/**'
    }),
    commonjs(),
    uglify()
  ],
  sourceMap: true,
  external: [
    'axios',
    'moment',
    'skiliko-sdk',
    'skiliko-sdk-js',
    'vue-analytics',
    'vue-datepicker',
    'vue-i18n',
    'vue-meta',
    'vue-router',
    'vuex'
  ],
  globals: {
    axios: 'axios',
    moment: 'moment',
    'skiliko-sdk': 'SkilikoSdk',
    'vue-analytics': 'VueAnalytics',
    'vue-i18n': 'VueI18n',
    'vue-meta': 'VueMeta',
    vuex: 'Vuex'
  }
}
