import { helpers } from 'skiliko-sdk'

const formatDate = (date, format) => {
  let dateStr = format
  const formatNumber = number => number < 10 ? `0${number}` : number.toString()
  const bindings = {
    YYYY: date => date.getFullYear(),
    MM: date => formatNumber(date.getMonth() + 1),
    DD: date => formatNumber(date.getDate()),
    HH: date => formatNumber(date.getHours()),
    m: date => formatNumber(date.getMinutes()),
    s: date => formatNumber(date.getSeconds())
  }
  Object.keys(bindings)
  .forEach(key => (dateStr = dateStr.replace(key, bindings[key](date))))
  return dateStr
}

const date = dateStr => formatDate(helpers.DateFromJson(dateStr), 'DD/MM/YYYY')

const time = dateStr => formatDate(helpers.DateFromJson(dateStr), 'DD/MM/YYYY HH:m')

const timeOnly = dateStr => formatDate(helpers.DateFromJson(dateStr), 'HHhm')

const hours = (minutes, compact = false, keepZero = false) => {
  const hours = parseInt(minutes / 60, 10)
  return [
    { value: hours, key: 'h' },
    { value: `0${minutes % 60}`.slice(-2), key: 'min' }
  ]
  .filter(e => keepZero ? true : parseInt(e.value, 10) > 0)
  .map(e => compact ? e.value : [e.value, e.key].join(''))
  .join(compact ? ':' : ' ')
}

const minutes = (seconds, withSec = false) => {
  const minutes = parseInt(seconds / 60, 10)
  if (!withSec) { return minutes }
  return [
    minutes,
    `0${seconds % 60}`.slice(-2)
  ].join(':')
}

const price = (price, currency = 'EUR') => helpers.FormatPrice(price, currency)

const newline = text => helpers.Newline(text)

export default {
  date,
  time,
  timeOnly,
  hours,
  minutes,
  price,
  newline
}
