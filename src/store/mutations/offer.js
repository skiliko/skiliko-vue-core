import { mergeArray } from './_merge'

const updateOffer = (state, offer) => {
  state.offers = mergeArray(state.offers, offer, (a, b) => a.price - b.price)
}

const updateCustomOffers = (state, offers) => {
  offers.forEach(offer => updateOffer(state, offer))
}

export default {
  updateOffer,
  updateCustomOffers
}
