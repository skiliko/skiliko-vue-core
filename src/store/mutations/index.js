import * as _ from './_keys'
import _chat from './chat'
import _purchase from './purchase'
import _expert from './expert'
import _offer from './offer'
import _feedback from './feedback'

export default {
  [_.PLATFORM_FETCHED]: (state, platform) => (state.platform = platform),
  [_.PLATFORM_FETCH_ERROR]: (state, error) => (state.globalError = error.response && error.response.data),
  [_.USER_FETCHED]: (state, user) => (state.currentUser = { ...state.currentUser, ...user }),
  [_.LOADING_START]: state => (state.isLoading = true),
  [_.LOADING_STOP]: state => (state.isLoading = false),
  [_.SIGNED_IN]: state => {
    state.isConnected = true
    state.currentUser = null
  },
  [_.SIGNED_OUT]: state => {
    state.isConnected = false
    state.currentUser = null
  },
  [_.PROMISE_CREATED]: (state, { name, promise }) => (state.promises[name] = promise),
  [_.COUNTRIES_FETCHED]: (state, countries) => (state.countries = countries),
  [_.CHAT_CREATED]: (state, chat) => (state.chat = chat),
  [_.CONVERSATIONS_UPDATED]: (state, { conversations, notification = false }) => _chat.conversationsUpdated(state, conversations, notification),
  [_.MESSAGE_RECEIVED]: (state, { message, notification = false }) => _chat.messagesReceived(state, message.conversation, [message], notification),
  [_.MESSAGES_RECEIVED]: (state, { messages, conversation, notification = false }) => _chat.messagesReceived(state, conversation, messages, notification),
  [_.REMOVE_PENDING_CHAT]: (state, messageIds) => _chat.removePending(state, messageIds),
  [_.PURCHASES_FETCHED]: (state, purchases) => _purchase.updatePurchases(state, purchases),
  [_.PURCHASES_PAYED]: (state, purchase) => _purchase.payedPurchase(state, purchase),
  [_.EXPERTS_FETCHED]: (state, experts) => _expert.updateExperts(state, experts),
  [_.OFFER_FETCHED]: (state, newOffer) => _offer.updateOffer(state, newOffer),
  [_.CUSTOM_OFFER_FETCHED]: (state, offers) => _offer.updateCustomOffers(state, offers),
  [_.FEEDBACKS_FETCHED]: (state, { expert, feedbacks }) => _feedback.updateFeedbacks(state, expert, feedbacks),
  [_.LANGUAGES_FETCHED]: (state, languages) => (state.languages = languages)
}
