const updateConversation = (state, conversation, messages, key) => {
  const conversationInCache = state[key][conversation._id] || {}
  const messagesInCache = (conversationInCache.messages || [])
  .filter(message => !messages.find(m => m._id === message._id))
  const newMessages = [
    ...messages,
    ...messagesInCache
  ]
  .sort((a, b) => a.sentAt.localeCompare(b.sentAt))
  newMessages.pagination = messages.pagination || (conversationInCache.messages || []).pagination
  state[key] = {
    ...state[key],
    [conversation._id]: {
      ...state[key][conversation._id] || {},
      ...conversation,
      messages: newMessages
    }
  }
}

const removeMessage = (state, messageIds, key) => {
  const conversations = Object.assign({}, state[key])
  const newConversations = {}
  for (const conversationId in conversations) {
    if (conversations.hasOwnProperty(conversationId)) {
      conversations[conversationId].messages = conversations[conversationId]
      .messages.filter(message => messageIds.indexOf(message._id) < 0)
      if (conversations[conversationId].messages.length > 0) {
        newConversations[conversationId] = conversations[conversationId]
      }
    }
  }
  state[key] = newConversations
}

const onReceived = (state, conversation, messages, notification = false) => {
  updateConversation(state, { _id: conversation }, messages, 'conversations')
  if (!notification) { return }
  const newMessages = messages.filter(m => m.from._id !== state.currentUser.chatable_id)
  if (newMessages.length === 0) { return }
  updateConversation(state, { _id: conversation }, newMessages, 'pendingConversations')
}

const messagesReceived = (state, conversation, messages, notification) => {
  if (!Array.isArray(messages)) { messages = [messages] }
  if (messages.pagination) {
    onReceived(state, conversation, messages, notification)
  } else {
    messages.forEach(message => onReceived(state, message.conversation, [message], notification))
  }
}

const conversationsUpdated = (state, conversations, notification) => {
  if (!Array.isArray(conversations)) { conversations = [conversations] }
  conversations.forEach(conversation => {
    updateConversation(state, conversation, [], 'conversations')
    if (notification) {
      updateConversation(state, conversation, [], 'pendingConversations')
    }
  })
}

const removePending = (state, messageIds) => {
  removeMessage(state, messageIds, 'pendingConversations')
}

export default {
  messagesReceived,
  conversationsUpdated,
  removePending
}
