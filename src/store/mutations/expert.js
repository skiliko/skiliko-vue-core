import offer from './offer'
import { mergeArray } from './_merge'

const canUpdate = expert => {
  if (expert.role === undefined) { return true }
  return [
    'active',
    'restricted'
  ].indexOf(expert.role) >= 0
}

const updateExpert = (state, expert) => {
  if (!canUpdate(expert)) { return }
  (expert.offers || []).forEach(o => offer.updateOffer(state, o))
  state.experts = mergeArray(state.experts, expert, (p1, p2) => p1.priority - p2.priority)
}

const updateExperts = (state, experts) => experts.forEach(expert => updateExpert(state, expert))

export default {
  updateExpert,
  updateExperts
}
