const updatePurchase = (state, purchase) => {
  state.purchases = [
    ...state.purchases.filter(p => p.id !== purchase.id),
    purchase
  ].sort((p1, p2) => p2.payed_at && p2.payed_at.localeCompare(p1.payed_at))
}

const updatePurchases = (state, purchases) => purchases.forEach(purchase => updatePurchase(state, purchase))

const payedPurchase = (state, purchase) => {
  updatePurchase(state, purchase)
  state.hasPurchasePayed = true
}

export default {
  updatePurchase,
  updatePurchases,
  payedPurchase
}
