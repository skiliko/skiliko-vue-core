const mergeArray = (array, element, sortFunction = () => null) => [
  ...array.filter(e => parseInt(e.id, 10) !== parseInt(element.id, 10)),
  {
    ...array.find(e => parseInt(e.id, 10) === parseInt(element.id, 10)),
    ...element
  }
].sort(sortFunction)

export {
  mergeArray
}
