import expertMutations from './expert'

const updateFeedbacks = (state, expertId, feedbacks) => {
  const expert = state.experts.filter(e => parseInt(e.id, 10) === parseInt(expertId, 10))[0]
  if (expert.feedbacks && expert.feedbacks.pagination.page > feedbacks.pagination.page) { return }
  expert.feedbacks = [...expert.feedbacks, ...feedbacks]
  .filter((value, index, self) => value && self.indexOf(value) === index)
  .sort((a, b) => new Date(b.created_at) - new Date(a.created_at))
  expert.feedbacks.pagination = feedbacks.pagination
  expertMutations.updateExpert(state, expert)
}

export default {
  updateFeedbacks
}
