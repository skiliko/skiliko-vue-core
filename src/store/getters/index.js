import { Settings } from 'skiliko-sdk'
import * as _ from './_keys'

const purchases = (state, filter = () => true) => state.purchases
  .filter(filter)

const offers = (state, expert, filter = () => true) => state.offers
  .filter(filter)
  .filter(o => expert ? o.admin_ids.indexOf(parseInt(expert.id, 10)) >= 0 : true)

export default {
  [_.PLATFORM]: state => state.platform,
  [_.GLOBAL_ERROR]: state => state.globalError,
  [_.ADMINISTRATOR]: state => state.platform.administrator,
  [_.EXPERTS]: state => state.experts,
  [_.SKILIKO_LOADING]: state => state.isLoading,
  [_.SIGNED_IN]: state => state.isConnected,

  [_.CURRENT_EMAIL]: () => Settings.get('email'),
  [_.CURRENT_USER]: state => state.currentUser,
  [_.OFFERS]: state => expert => offers(state, expert),
  [_.PREPAID_OFFERS]: state => () => offers(state, null, e => e.service_type === 'prepaid_minute'),
  [_.SERVICE_OFFERS]: state => expert => offers(state, expert, e => e.service_type === 'service'),
  [_.MAIL_OFFERS]: state => expert => offers(state, expert, e => e.service_type === 'mail'),
  [_.CALL_OFFERS]: state => expert => offers(state, expert, e => e.service_type === 'phone'),
  [_.HOROSCOPE]: state => offers(state, null, e => e.service_type === 'horoscope'),
  [_.PORTRAIT]: state => offers(state, null, e => e.service_type === 'portrait'),
  [_.PAGES]: state => offers(state, null, e => e.service_type === 'page'),

  [_.PROMISES]: state => state.promises,
  [_.COUNTRIES]: state => state.countries,
  [_.LANGUAGES]: state => state.languages,
  [_.CHAT]: state => state.chat,
  [_.PENDING_CONVERSATIONS_COUNT]: state => Object.keys(state.pendingConversations).length,
  [_.CONVERSATIONS]: state => state.conversations,

  [_.PURCHASES]: state => state.purchases,
  [_.PURCHASES_BY_STATUS]: state => ({
    finished: purchases(state, purchase => !!purchase.finished_at),
    current: purchases(state, purchase => !!purchase.accepted_at && !purchase.finished_at),
    pending: purchases(state, purchase => !purchase.accepted_at)
  }),

  [_.OFFER]: state => id => state.offers.find(o => parseInt(o.id, 10) === parseInt(id, 10)),

  [_.CHAT_AVAILABLE]: state => {
    if (state.hasPurchasePayed) { return true }
    if (!state.currentUser) { return false }
    return !![
      state.currentUser.phone_seconds_available,
      state.currentUser.stats.purchases.pending,
      state.currentUser.stats.purchases.current,
      state.currentUser.stats.purchases.count,
      state.currentUser.stats.purchases.to_rate
    ].find(stat => stat > 0)
  }
}
