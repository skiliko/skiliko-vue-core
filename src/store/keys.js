import * as __MUTATIONS__ from './mutations/_keys'
import * as __GETTERS__ from './getters/_keys'
import * as __ACTIONS__ from './actions/_keys'

export const MUTATIONS = __MUTATIONS__
export const GETTERS = __GETTERS__
export const ACTIONS = __ACTIONS__
