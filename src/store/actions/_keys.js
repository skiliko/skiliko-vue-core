export const CACHE_PROMISE = 'CACHE_PROMISE'

export const CURRENT_PLATFORM = 'CURRENT_PLATFORM'
export const FETCH_PLATFORM = 'FETCH_PLATFORM'
export const SIGNIN = 'SIGNIN'
export const SIGNUP = 'SIGNUP'
export const SIGNOUT = 'SIGNOUT'
export const CANCEL_ACCOUNT = 'CANCEL_ACCOUNT'
export const SEND_PASSWORD_MAIL = 'SEND_PASSWORD_MAIL'
export const RESET_PASSWORD = 'RESET_PASSWORD'
export const CHANGE_PASSWORD = 'CHANGE_PASSWORD'
export const CURRENT_USER = 'CURRENT_USER'
export const FETCH_CURRENT_USER = 'FETCH_CURRENT_USER'
export const SAVE_USER = 'SAVE_USER'
export const FETCH_COUNTRIES = 'FETCH_COUNTRIES'
export const CREATE_CARD = 'CREATE_CARD'
export const INIT_CHAT = 'INIT_CHAT'
export const CHAT = 'CHAT'
export const FETCH_PENDING_MESSAGES = 'FETCH_PENDING_MESSAGES'
export const CONVERSATION = 'CONVERSATION'
export const FETCH_CONVERSATIONS = 'FETCH_CONVERSATIONS'
export const FETCH_MESSAGES = 'FETCH_MESSAGES'
export const SEND_CHAT_MESSAGE = 'SEND_CHAT_MESSAGE'
export const MARK_MESSAGE_AS_READ = 'MARK_MESSAGE_AS_READ'
export const MARK_MESSAGE_AS_RECEIVED = 'MARK_MESSAGE_AS_RECEIVED'
export const CREATE_PURCHASE = 'CREATE_PURCHASE'
export const FETCH_PURCHASES = 'FETCH_PURCHASES'
export const GET_PURCHASE = 'GET_PURCHASE'
export const PAY_PURCHASE = 'PAY_PURCHASE'
export const RATE_PURCHASE = 'RATE_PURCHASE'
export const ACCEPT_PURCHASE = 'ACCEPT_PURCHASE'
export const REFUND_PURCHASE = 'REFUND_PURCHASE'
export const SAVE_SERVICE = 'SAVE_SERVICE'
export const UPLOAD = 'UPLOAD'
export const FETCH_EXPERTS = 'FETCH_EXPERTS'
export const GET_EXPERT = 'GET_EXPERT'
export const FETCH_OFFER = 'FETCH_OFFER'
export const FETCH_CUSTOM_OFFERS = 'FETCH_CUSTOM_OFFERS'
export const FETCH_FEEDBACKS = 'FETCH_FEEDBACKS'
export const FETCH_LANGUAGES = 'FETCH_LANGUAGES'
export const SUPPORT = 'SUPPORT'
export const REQUEST_APPOINTMENT = 'REQUEST_APPOINTMENT'
