import { Country } from 'skiliko-sdk'
import { MUTATIONS, ACTIONS } from '../keys'
import api from './_api'

const all = ({ commit, dispatch }, locale) => dispatch(ACTIONS.CACHE_PROMISE, {
  name: 'countries',
  promise: () => api(Country.all(locale), commit)
  .then(countries => {
    commit(MUTATIONS.COUNTRIES_FETCHED, countries)
    return countries
  })
})

export default {
  [ACTIONS.FETCH_COUNTRIES]: all
}
