import { Purchase, Rating } from 'skiliko-sdk'
import { ACTIONS, MUTATIONS } from '../keys'
import api from './_api'

const fetch = ({ commit, dispatch }, { scope = null, page = 1 }) => dispatch(ACTIONS.CURRENT_PLATFORM)
  .then(() => api(Purchase.all(scope, page), commit))
  .then(purchases => {
    commit(MUTATIONS.PURCHASES_FETCHED, purchases)
    return purchases
  })

const get = ({ commit, dispatch }, id) => dispatch(ACTIONS.CURRENT_PLATFORM)
  .then(() => api(Purchase.get(id), commit))
  .then(purchase => {
    commit(MUTATIONS.PURCHASES_FETCHED, [purchase])
    return purchase
  })

const create = ({ commit }, { offer, expert }) => api(Purchase.create(offer, expert), commit)
  .then(purchase => {
    commit(MUTATIONS.PURCHASES_FETCHED, [purchase])
    return purchase
  })

const pay = ({ commit, dispatch }, { purchase, card }) => {
  purchase.card_id = card.id
  return api(Purchase.pay(purchase), commit)
  .then(purchase => {
    commit(MUTATIONS.PURCHASES_PAYED, purchase)
    commit(MUTATIONS.USER_FETCHED, purchase.user)
    return purchase
  })
}

const rate = ({ commit, dispatch }, { purchase, note, content }) => api(Rating.create(purchase.id, note, content), commit)
  .then(() => dispatch(ACTIONS.GET_PURCHASE, purchase.id))

const accept = ({ commit, dispatch }, purchase) => api(Purchase.accept(purchase), commit)
  .then(purchase => {
    commit(MUTATIONS.PURCHASES_FETCHED, [purchase])
    return purchase
  })

const refund = ({ commit, dispatch }, purchase) => api(Purchase.refund(purchase), commit)
  .then(purchase => {
    commit(MUTATIONS.PURCHASES_FETCHED, [purchase])
    return purchase
  })

const saveService = ({ commit, dispatch }, { purchase, data }) => {
  commit(MUTATIONS.PURCHASES_FETCHED, [purchase])
  return api(Purchase.save(purchase, data), commit)
  .then(newPurchase => {
    commit(MUTATIONS.PURCHASES_FETCHED, [newPurchase])
    return newPurchase
  })
}

export default {
  [ACTIONS.CREATE_PURCHASE]: create,
  [ACTIONS.FETCH_PURCHASES]: fetch,
  [ACTIONS.GET_PURCHASE]: get,
  [ACTIONS.PAY_PURCHASE]: pay,
  [ACTIONS.RATE_PURCHASE]: rate,
  [ACTIONS.ACCEPT_PURCHASE]: accept,
  [ACTIONS.REFUND_PURCHASE]: refund,
  [ACTIONS.SAVE_SERVICE]: saveService
}
