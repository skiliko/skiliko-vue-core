import axios from 'axios'
import { ACTIONS, MUTATIONS } from '../keys'

const fetch = ({ dispatch, commit }) => dispatch(ACTIONS.CACHE_PROMISE, {
  name: 'languages',
  promise: () => axios('https://skiliko.firebaseio.com/languages.json')
  .then(response => response.data)
  .then(languages => {
    commit(MUTATIONS.LANGUAGES_FETCHED, languages)
    return languages
  })
})

export default {
  [ACTIONS.FETCH_LANGUAGES]: fetch
}
