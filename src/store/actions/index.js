import cache from './cache'
import user from './user'
import platform from './platform'
import session from './session'
import registration from './registration'
import password from './password'
import country from './country'
import card from './card'
import chat from './chat'
import purchase from './purchase'
import upload from './upload'
import expert from './expert'
import offer from './offer'
import feedback from './feedback'
import language from './language'
import support from './support'
import appointment from './appointment'

export default {
  ...cache,
  ...platform,
  ...card,
  ...session,
  ...registration,
  ...password,
  ...user,
  ...country,
  ...chat,
  ...purchase,
  ...upload,
  ...expert,
  ...offer,
  ...feedback,
  ...language,
  ...support,
  ...appointment
}
