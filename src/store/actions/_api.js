import { MUTATIONS } from '../keys'

const api = (promise, commit) => new Promise((resolve, reject) => {
  commit(MUTATIONS.LOADING_START)
  promise.then(data => {
    commit(MUTATIONS.LOADING_STOP)
    resolve(data)
  })
  .catch(error => {
    commit(MUTATIONS.LOADING_STOP)
    const e = (error.response && error.response.data) || error || {}
    e.response = error.response
    reject(e)
  })
})

export default api
