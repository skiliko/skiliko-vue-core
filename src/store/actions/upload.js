import { Upload } from 'skiliko-sdk'
import { ACTIONS } from '../keys'

const send = ({ commit, dispatch }, { files, path }) => {
  const fileList = []
  for (let i = 0; i < files.length; i++) {
    fileList.push(files[i])
  }
  return Promise.all(fileList.map(file => Upload.upload(file, path)))
}

export default {
  [ACTIONS.UPLOAD]: send
}
