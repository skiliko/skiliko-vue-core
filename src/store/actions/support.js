import { Support } from 'skiliko-sdk'
import { ACTIONS } from '../keys'
import api from './_api'

const send = ({ commit, dispatch }, { email, message, title }) => api(Support.message({
  email,
  message,
  title
}), commit)

export default {
  [ACTIONS.SUPPORT]: send
}
