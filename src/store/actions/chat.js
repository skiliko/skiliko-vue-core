import { Chat } from 'skiliko-sdk'
import { MUTATIONS, ACTIONS, GETTERS } from '../keys'
import api from './_api'

const chat = ({ commit, dispatch }) => dispatch(ACTIONS.CACHE_PROMISE, {
  name: 'chat',
  promise: () => dispatch(ACTIONS.CURRENT_USER)
  .then(user => api(Chat.getChat(user.chatable_id), commit))
})

const init = ({ commit, dispatch, getters }) => {
  getters[GETTERS.PROMISES].chat = null
  dispatch(ACTIONS.CHAT)
  .then(chat => {
    chat.onNewMessage(message => {
      commit(MUTATIONS.MESSAGE_RECEIVED, { message, notification: true })
      dispatch(ACTIONS.MARK_MESSAGE_AS_RECEIVED, message)
    })
    .onMessageReceived(message => commit(MUTATIONS.MESSAGE_RECEIVED, { message }))
    .onMessageRead(message => commit(MUTATIONS.MESSAGE_RECEIVED, { message }))
    .onNewConversation(conversation => commit(MUTATIONS.CONVERSATIONS_UPDATED, { conversations: conversation }))
    return chat
  })
  .then(chat => commit(MUTATIONS.CHAT_CREATED, chat))
  .then(() => dispatch(ACTIONS.FETCH_PENDING_MESSAGES))
}

const pendingMessages = ({ commit, dispatch }) => dispatch(ACTIONS.CURRENT_USER)
  .then(user => api(Chat.pendingMessages(user.chatable_id), commit))
  .then(messages => {
    commit(MUTATIONS.MESSAGES_RECEIVED, {
      messages,
      notification: true
    })
    messages.forEach(message => dispatch(ACTIONS.MARK_MESSAGE_AS_RECEIVED, message))
    return messages
  })

const conversation = ({ dispatch, commit }, conversationWith) => dispatch(ACTIONS.CHAT)
  .then(chat => api(chat.conversationWith(conversationWith), commit))

const conversations = ({ dispatch, commit }, page = 1) => dispatch(ACTIONS.CHAT)
  .then(chat => api(chat.conversations(page), commit))
  .then(conversations => {
    commit(MUTATIONS.CONVERSATIONS_UPDATED, { conversations })
    return conversations
  })

const messages = ({ dispatch, commit }, { conversationWith, page = 1 }) => dispatch(ACTIONS.CHAT)
  .then(chat => dispatch(ACTIONS.CONVERSATION, conversationWith)
    .then(conversation => api(chat.messages(conversation._id, page), commit)
      .then(messages => {
        commit(MUTATIONS.MESSAGES_RECEIVED, {
          messages,
          conversation: conversation._id,
          nofification: false
        })
        messages.forEach(message => dispatch(ACTIONS.MARK_MESSAGE_AS_RECEIVED, message))
        return messages
      })
    )
  )

const send = ({ dispatch, commit }, { message, to, data }) => dispatch(ACTIONS.CHAT)
  .then(chat => api(chat.send(message, to, data), commit))

const read = ({ dispatch, commit }, messages) => dispatch(ACTIONS.CHAT)
  .then(chat => chat.markAsRead(messages))
  .then(() => commit(MUTATIONS.REMOVE_PENDING_CHAT, messages))

const receive = ({ dispatch, commit }, message) => dispatch(ACTIONS.CHAT)
  .then(chat => {
    if (message.from._id !== chat.from && !message.receivedAt) {
      chat.markAsReceived(message._id)
    }
  })

export default {
  [ACTIONS.INIT_CHAT]: init,
  [ACTIONS.CHAT]: chat,
  [ACTIONS.CONVERSATION]: conversation,
  [ACTIONS.FETCH_CONVERSATIONS]: conversations,
  [ACTIONS.FETCH_PENDING_MESSAGES]: pendingMessages,
  [ACTIONS.FETCH_MESSAGES]: messages,
  [ACTIONS.SEND_CHAT_MESSAGE]: send,
  [ACTIONS.MARK_MESSAGE_AS_READ]: read,
  [ACTIONS.MARK_MESSAGE_AS_RECEIVED]: receive
}
