import { Offer } from 'skiliko-sdk'
import { ACTIONS, MUTATIONS } from '../keys'
import api from './_api'

const fetch = ({ commit, dispatch }, id) => dispatch(ACTIONS.CURRENT_PLATFORM)
  .then(() => api(Offer.get(id), commit))
  .then(offer => {
    commit(MUTATIONS.OFFER_FETCHED, offer)
    return offer
  })

const fetchCustomOffers = ({ commit, dispatch }) => dispatch(ACTIONS.CURRENT_PLATFORM)
  .then(() => api(Offer.customOffers(), commit))
  .then(offers => {
    commit(MUTATIONS.CUSTOM_OFFER_FETCHED, offers)
    return offers
  })

export default {
  [ACTIONS.FETCH_OFFER]: fetch,
  [ACTIONS.FETCH_CUSTOM_OFFERS]: fetchCustomOffers
}
