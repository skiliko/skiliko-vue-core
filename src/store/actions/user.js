import { User } from 'skiliko-sdk'
import { MUTATIONS, GETTERS, ACTIONS } from '../keys'
import api from './_api'

const user = ({ commit, dispatch }) => dispatch(ACTIONS.CACHE_PROMISE, {
  name: 'user',
  promise: () => dispatch(ACTIONS.CURRENT_PLATFORM)
  .then(() => api(User.current({ forceFetch: true }), commit))
  .then(user => {
    commit(MUTATIONS.USER_FETCHED, user)
    return user
  })
})

const save = ({ commit }, data) => api(User.save(data), commit)
  .then(user => {
    commit(MUTATIONS.USER_FETCHED, user)
    return user
  })

const fetch = ({ commit, getters, dispatch }, force = false) => {
  if (force) { getters[GETTERS.PROMISES].user = null }
  return dispatch(ACTIONS.CURRENT_USER)
}

export default {
  [ACTIONS.SAVE_USER]: save,
  [ACTIONS.CURRENT_USER]: user,
  [ACTIONS.FETCH_CURRENT_USER]: fetch
}
