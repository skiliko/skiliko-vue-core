import { Platform } from 'skiliko-sdk'
import { MUTATIONS, ACTIONS, GETTERS } from '../keys'
import api from './_api'

const platform = ({ commit, dispatch }) => dispatch(ACTIONS.CACHE_PROMISE, {
  name: 'platform',
  promise: () => api(Platform.current(), commit)
})

const fetch = ({ commit, getters, dispatch }, force) => {
  if (force) { getters[GETTERS.PROMISES].platform = null }
  return dispatch(ACTIONS.CURRENT_PLATFORM)
  .then(platform => {
    commit(MUTATIONS.PLATFORM_FETCHED, platform)
    platform.offers.forEach(offer => commit(MUTATIONS.OFFER_FETCHED, offer))
    return platform
  })
}

export default {
  [ACTIONS.CURRENT_PLATFORM]: platform,
  [ACTIONS.FETCH_PLATFORM]: fetch
}
