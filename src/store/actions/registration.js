import { Session, Settings } from 'skiliko-sdk'
import { ACTIONS, MUTATIONS } from '../keys'
import api from './_api'

const create = ({ commit, dispatch }, data) => {
  Settings.set('email', data.email)
  return api(Session.register(data), commit)
  .then(() => dispatch(ACTIONS.SIGNIN))
}

const cancel = ({ commit }) => api(Session.unregister(), commit)
  .then(() => commit(MUTATIONS.SIGNED_OUT))

export default {
  [ACTIONS.SIGNUP]: create,
  [ACTIONS.CANCEL_ACCOUNT]: cancel
}
