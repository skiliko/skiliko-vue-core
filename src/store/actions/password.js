import { Settings, Password } from 'skiliko-sdk'
import { ACTIONS } from '../keys'
import api from './_api'

const mail = ({ commit }, email) => {
  Settings.set('email', email)
  return api(Password.sendInstructions(email), commit)
}

const reset = ({ commit }, data) => api(Password.reset(data.token, data.password, data.passwordConfirmation), commit)

const update = ({ dispatch, commit }, data) => dispatch(ACTIONS.SAVE_USER, data)

export default {
  [ACTIONS.SEND_PASSWORD_MAIL]: mail,
  [ACTIONS.RESET_PASSWORD]: reset,
  [ACTIONS.CHANGE_PASSWORD]: update
}
