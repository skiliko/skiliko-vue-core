import { Session, Settings } from 'skiliko-sdk'
import { MUTATIONS, ACTIONS } from '../keys'
import api from './_api'

const create = ({ commit, dispatch }, data = {}) => {
  let promise = null
  if (Session.signedIn()) {
    promise = Promise.resolve()
  } else {
    Settings.set('email', data.email)
    promise = Session.login(data.email, data.password)
  }
  return api(promise, commit)
  .then(() => commit(MUTATIONS.SIGNED_IN))
  .then(() => dispatch(ACTIONS.FETCH_CURRENT_USER, true))
  .then(() => dispatch(ACTIONS.INIT_CHAT))
}

const cancel = ({ commit }) => api(Session.logout(), commit)
  .then(() => commit(MUTATIONS.SIGNED_OUT))

export default {
  [ACTIONS.SIGNIN]: create,
  [ACTIONS.SIGNOUT]: cancel
}
