import { ACTIONS, MUTATIONS, GETTERS } from '../keys'

const cache = ({ getters, commit }, { promise, name }) => {
  if (getters[GETTERS.PROMISES][name]) { return getters[GETTERS.PROMISES][name] }
  const p = promise()
  commit(MUTATIONS.PROMISE_CREATED, { promise: p, name })
  return p
}

export default {
  [ACTIONS.CACHE_PROMISE]: cache
}
