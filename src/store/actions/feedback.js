import { Rating } from 'skiliko-sdk'
import { MUTATIONS, ACTIONS } from '../keys'
import api from './_api'

const fetch = ({ commit, dispatch }, { expert, page = 1 }) => dispatch(ACTIONS.CURRENT_PLATFORM)
  .then(() => api(Rating.all(page, expert), commit))
  .then(feedbacks => {
    commit(MUTATIONS.FEEDBACKS_FETCHED, {
      feedbacks,
      expert
    })
    return feedbacks
  })

export default {
  [ACTIONS.FETCH_FEEDBACKS]: fetch
}
