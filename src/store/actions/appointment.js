import { Timezone } from 'skiliko-sdk'
import { ACTIONS } from '../keys'
import filters from '../../filters'

const generateAppointmentMessage = (appointment, purchase, translator) => {
  const timezone = Timezone.detect()
  const timezoneOffset = filters.hours(Math.abs(timezone.offset), true, true)
  const timezoneOffsetStr = ` <small>(GMT${timezone.offset >= 0 ? '+' : '-'}${timezoneOffset})</small>`
  const availabilities = appointment.availabilities.map(availability => {
    const date = filters.date(availability.day)
    const from = filters.hours(availability.from, true, true)
    const to = filters.hours(availability.to, true, true)
    return translator('availability', { date, from, to }) + timezoneOffsetStr
  }).join('<br/>')
  return translator('appointmentText', {
    availabilities,
    nickname: purchase.admin.nickname,
    duration: appointment.duration,
    subject: appointment.subject
  })
}

const requestAppointment = ({ dispatch }, { appointment, expert, purchase, translator }) => {
  if (expert.isRestricted()) {
    throw new Error('This expert is actually restricted and cannot accept appointment')
  }
  return dispatch(ACTIONS.CHAT)
  .then(() => dispatch(ACTIONS.SEND_CHAT_MESSAGE, {
    message: generateAppointmentMessage(appointment, purchase, translator),
    to: purchase.admin.chatable_id,
    data: {
      ...appointment,
      type: 'appointment'
    }
  }))
}

export default {
  [ACTIONS.REQUEST_APPOINTMENT]: requestAppointment
}
