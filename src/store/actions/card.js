import { Card } from 'skiliko-sdk'
import { ACTIONS } from '../keys'
import api from './_api'

const create = ({ commit, dispatch }, card) => api(
  Card.create(card.number, card.month, card.year, card.cvx),
  commit
)
.then(card => {
  if (card.status === 'ERROR') {
    return Promise.reject(card)
  }
  return dispatch(ACTIONS.SAVE_USER, { card_id: card.id })
  .then(() => card)
})

export default {
  [ACTIONS.CREATE_CARD]: create
}
