import { Expert } from 'skiliko-sdk'
import { ACTIONS, MUTATIONS } from '../keys'
import api from './_api'

const fetch = ({ commit, dispatch }, { page = 1 }) => dispatch(ACTIONS.CURRENT_PLATFORM)
  .then(() => api(Expert.all(page), commit))
  .then(experts => {
    commit(MUTATIONS.EXPERTS_FETCHED, experts)
    return experts
  })

const get = ({ commit, dispatch }, id) => dispatch(ACTIONS.CURRENT_PLATFORM)
  .then(() => api(Expert.get(id), commit))
  .then(expert => {
    commit(MUTATIONS.EXPERTS_FETCHED, [expert])
    return expert
  })

export default {
  [ACTIONS.FETCH_EXPERTS]: fetch,
  [ACTIONS.GET_EXPERT]: get
}
