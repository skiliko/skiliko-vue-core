import Vuex from 'vuex'
import actions from './actions'
import getters from './getters'
import mutations from './mutations'

const defaultState = {
  platform: { picture: {} },
  isLoading: false,
  isConnected: null,
  currentUser: null,
  chat: null,
  hasPurchasePayed: false,
  promises: {
    platform: null,
    user: null,
    countries: null,
    chat: null
  },
  conversations: {},
  pendingConversations: {},
  experts: [],
  purchases: [],
  countries: [],
  languages: {},
  offers: []
}

let store = null

export default () => {
  if (store) { return store }
  store = new Vuex.Store({
    state: defaultState,
    actions,
    mutations,
    getters
  })
  return store
}
