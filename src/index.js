import Vuex from 'vuex'
import VueMeta from 'vue-meta'
import VueI18n from 'vue-i18n'
import VueAnalytics from 'vue-analytics'
import moment from 'moment'
import { initForBrowser, initForNode, Settings, Session } from 'skiliko-sdk'
import * as keys from './store/keys'
import skiliko from './mixins/skiliko'
import storeInstance from './store'
import * as mixins from './mixins'

const initApplication = (Vue, platform, store, options) => {
  Session.init(platform.token)
  Settings.set('locale', platform.locale)
  moment.locale(platform.locale)
  const ids = [
    options.googleAnalytics,
    platform.ga_id
  ].filter(e => e)
  if (ids.length) {
    Vue.use(VueAnalytics, {
      router: options.router,
      id: ids,
      debug: { enabled: !options.trackViews }
    })
  }
  if (Session.signedIn()) {
    store.dispatch(keys.ACTIONS.SIGNIN)
  } else {
    store.dispatch(keys.ACTIONS.SIGNOUT)
  }
}

const restoreStoreFromServerData = () => {
  if (!window.__INITIAL_VUEX_STATE__) { return }
  const state = window.__INITIAL_VUEX_STATE__
  state.promises = {
    platform: state.platform ? Promise.resolve(state.platform) : null,
    user: state.currentUser ? Promise.resolve(state.currentUser) : null,
    countries: state.countries ? Promise.resolve(state.countries) : null,
    chat: state.chat ? Promise.resolve(state.chate) : null
  }
  storeInstance().replaceState(state)
}

const SkilikoVueCore = {
  install (Vue, options = {}) {
    Vue.use(Vuex)
    Vue.use(VueMeta)
    Vue.use(VueI18n)
    Vue.mixin(skiliko)

    Vue.config.lang = 'fr'
    Vue.locale(Vue.config.lang, options.locales)
    moment.locale(Vue.config.lang)

    if (options.request) {
      initForNode(options.request, options.domain)
    } else {
      initForBrowser(options.domain)
      restoreStoreFromServerData()
    }

    return storeInstance().dispatch(keys.ACTIONS.FETCH_PLATFORM)
    .then(platform => {
      // remove all google analytics for the server side
      if (options.request) {
        delete options.googleAnalytics
        delete platform.ga_id
      }
      initApplication(Vue, platform, storeInstance(), options)
    })
  }
}

export default SkilikoVueCore
export {
  storeInstance,
  mixins,
  keys
}
