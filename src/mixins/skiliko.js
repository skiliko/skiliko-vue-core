import { mapGetters } from 'vuex'
import { GETTERS } from '../store/keys'
import filters from '../filters'

export default {
  filters,
  computed: {
    ...mapGetters({
      platform: GETTERS.PLATFORM,
      currentEmail: GETTERS.CURRENT_EMAIL,
      currentUser: GETTERS.CURRENT_USER,
      isLoading: GETTERS.SKILIKO_LOADING,
      isSignedIn: GETTERS.SIGNED_IN,
      chat: GETTERS.CHAT,
      pendingConversationsCount: GETTERS.PENDING_CONVERSATIONS_COUNT,
      conversations: GETTERS.CONVERSATIONS,
      administrator: GETTERS.ADMINISTRATOR,
      experts: GETTERS.EXPERTS
    })
  }
}
