import { Settings } from 'skiliko-sdk'
import { ACTIONS } from '../store/keys'

export default {
  props: {
    purchase: {
      type: Object,
      required: true
    },
    expert: {
      type: Object,
      required: true
    }
  },
  data () {
    return {
      errors: {}
    }
  },
  methods: {
    dataKey (purchase) {
      return `purchase-${purchase.id}`
    },
    defaultData (purchase) {
      return Settings.get(this.dataKey(purchase), {})
    },
    saveService (purchase, data) {
      this.$set(this, 'errors', {})
      Settings.set(this.dataKey(purchase), data)
      return this.$store.dispatch(ACTIONS.SAVE_SERVICE, { purchase, data })
      .then(purchase => this.$emit('completed', purchase))
      .catch(error => {
        if (error.response.status === 401) {
          this.$router.push({ name: 'SignUp', query: { next: this.$router.currentRoute.path } })
        } else {
          this.$set(this, 'errors', error)
        }
      })
    }
  }
}
